
-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 29, 2017 at 07:37 AM
-- Server version: 5.7.16-0ubuntu0.16.04.1
-- PHP Version: 5.6.29-1+deb.sury.org~xenial+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `psconnect_dev2017`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ad_campaigns`
--

CREATE TABLE `tbl_ad_campaigns` (
  `id` int(255) NOT NULL,
  `institution_name` varchar(255) NOT NULL,
  `duration` varchar(255) NOT NULL,
  `ad_price_billed` varchar(255) NOT NULL,
  `ad_duration` varchar(255) NOT NULL,
  `ad_start_date` varchar(255) NOT NULL,
  `ad_end_date` varchar(255) NOT NULL,
  `user_id` bigint(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pub` tinyint(4) DEFAULT '1',
  `del` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ad_campaigns`
--

INSERT INTO `tbl_ad_campaigns` (`id`, `institution_name`, `duration`, `ad_price_billed`, `ad_duration`, `ad_start_date`, `ad_end_date`, `user_id`, `date`, `pub`, `del`) VALUES
(2, '89', '5', '10000', '2', '2017-04-04', '2017-04-30', 6, '2017-04-03 22:11:04', 1, 1),
(3, '774', '5', '15000', '', '14042017', '14062017', 6, '2017-04-04 09:49:49', 0, 0),
(4, '40', '5', '15000', '2', '2017-04-24', '2017-05-11', 6, '2017-04-13 11:13:35', 1, 1),
(5, '359', '6', '25000', '35', '2017-04-18', '2017-06-15', 6, '2017-04-16 16:16:01', 1, 1),
(6, '357', '5', '45000', '14', '2017-04-17', '2017-04-29', 6, '2017-04-16 16:37:56', 1, 1),
(7, '12', '15', '60000', '5', '2017-04-17', '2017-07-17', 1089, '2017-04-16 16:51:32', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ad_campaigns_images`
--

CREATE TABLE `tbl_ad_campaigns_images` (
  `id` int(255) NOT NULL,
  `ad_campaigns` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `user_id` bigint(255) NOT NULL,
  `view_counter` bigint(20) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pub` tinyint(4) DEFAULT '1',
  `del` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ad_campaigns_images`
--

INSERT INTO `tbl_ad_campaigns_images` (`id`, `ad_campaigns`, `location`, `image`, `user_id`, `view_counter`, `date`, `pub`, `del`) VALUES
(3, '2', '5', '20120718-IMG_9212.jpg', 6, 0, '2017-04-04 05:37:25', 1, 1),
(4, '2', '2', '14494802_1136168859821600_4689461139775169404_n.jpg', 6, 0, '2017-04-04 05:48:06', 1, 1),
(5, '3', '1', 'Financial Analyst.jpg', 6, 0, '2017-04-04 09:56:35', 0, 0),
(6, '3', '2', 'Administrator.jpg', 6, 0, '2017-04-04 10:00:55', 1, 1),
(7, '3', '4', 'Arbitrators mediators and conciliators.jpg', 6, 0, '2017-04-04 10:04:06', 1, 1),
(8, '4', '1', '433457.jpg', 6, 0, '2017-04-13 11:16:18', 1, 1),
(9, '6', '4', 'cfc1ef_81d8316c02f543bfa330f0a14ad1905c.jpg', 6, 0, '2017-04-16 16:40:16', 1, 1),
(10, '7', '5', 'cakefest.jpg', 6, 0, '2017-04-16 17:03:59', 0, 0),
(11, '7', '3', 'cityvarsity.jpg', 1089, 0, '2017-04-17 12:22:23', 1, 1),
(12, '7', '5', 'Artist.jpg', 1083, 0, '2017-04-18 09:46:10', 0, 0),
(13, '5', '6', 'Arbitrators mediators and conciliators.jpg', 6, 0, '2017-04-20 10:44:35', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ad_campaign_locations`
--

CREATE TABLE `tbl_ad_campaign_locations` (
  `id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pub` tinyint(4) DEFAULT '1',
  `del` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_ad_campaign_locations`
--

INSERT INTO `tbl_ad_campaign_locations` (`id`, `name`, `date`, `pub`, `del`) VALUES
(1, 'Profile 728 x 90', '2017-04-16 11:44:25', 1, 1),
(2, 'Dashboard - 300 x 250', '2017-04-16 11:44:25', 1, 1),
(3, 'Institution index (clickable) - 728 x 90', '2017-04-16 11:45:03', 1, 1),
(4, 'Institution View - 728 x 90', '2017-04-16 11:45:03', 1, 1),
(5, 'Career Index - 728 x 90', '2017-04-16 11:45:27', 1, 1),
(6, 'Faculties Index - 728 x 90', '2017-04-16 11:45:27', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_campaign_ad_packages`
--

CREATE TABLE `tbl_campaign_ad_packages` (
  `id` int(255) NOT NULL,
  `campaign` varchar(255) NOT NULL,
  `duration` varchar(255) NOT NULL,
  `price` varchar(255) NOT NULL,
  `user_id` bigint(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pub` tinyint(4) DEFAULT '1',
  `del` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_campaign_ad_packages`
--

INSERT INTO `tbl_campaign_ad_packages` (`id`, `campaign`, `duration`, `price`, `user_id`, `date`, `pub`, `del`) VALUES
(4, '2', '5', '25000', 6, '2017-04-03 00:51:21', 0, 0),
(5, '2', '3', '15000', 6, '2017-04-03 01:09:45', 1, 1),
(6, '4', '3', '25000', 6, '2017-04-04 09:33:02', 1, 1),
(7, '4', '6', '23000', 6, '2017-04-04 09:34:00', 1, 1),
(8, '4', '9', '21000', 6, '2017-04-04 09:34:28', 1, 1),
(9, '4', '12', '20000', 6, '2017-04-04 09:34:58', 1, 1),
(10, '2', '6', '13000', 6, '2017-04-04 09:35:32', 1, 1),
(11, '2', '9', '11000', 6, '2017-04-04 09:35:50', 1, 1),
(12, '2', '12', '10000', 6, '2017-04-04 09:36:08', 1, 1),
(13, '5', '1', '1000', 6, '2017-04-13 11:03:59', 1, 1),
(14, '8', '3', '10000', 6, '2017-04-16 16:49:56', 1, 1),
(15, '8', '4', '15000', 6, '2017-04-16 16:50:12', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_campaign_packages`
--

CREATE TABLE `tbl_campaign_packages` (
  `id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `user_id` bigint(255) NOT NULL,
  `location_options` varchar(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pub` tinyint(4) DEFAULT '1',
  `del` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_campaign_packages`
--

INSERT INTO `tbl_campaign_packages` (`id`, `name`, `user_id`, `location_options`, `date`, `pub`, `del`) VALUES
(1, '0', 6, '', '2017-04-02 23:11:36', 0, 0),
(2, 'SILVER', 6, '6,4', '2017-04-02 23:21:56', 1, 1),
(3, 'GUDDIEY', 6, '', '2017-04-02 23:58:27', 0, 0),
(4, 'GOLD', 6, '1,3,6', '2017-04-04 09:32:00', 1, 1),
(5, 'BRONZE', 6, '2,3,5', '2017-04-13 11:02:57', 1, 1),
(6, 'TESTING2', 6, '', '2017-04-16 12:01:29', 0, 0),
(7, 'TEST3', 6, '6,8,10', '2017-04-16 12:02:25', 0, 0),
(8, 'PLATINUM', 6, '3,4,5', '2017-04-16 16:49:19', 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_ad_campaigns`
--
ALTER TABLE `tbl_ad_campaigns`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tbl_ad_campaigns_images`
--
ALTER TABLE `tbl_ad_campaigns_images`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tbl_ad_campaign_locations`
--
ALTER TABLE `tbl_ad_campaign_locations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tbl_campaign_ad_packages`
--
ALTER TABLE `tbl_campaign_ad_packages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tbl_campaign_packages`
--
ALTER TABLE `tbl_campaign_packages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_ad_campaigns`
--
ALTER TABLE `tbl_ad_campaigns`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tbl_ad_campaigns_images`
--
ALTER TABLE `tbl_ad_campaigns_images`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `tbl_ad_campaign_locations`
--
ALTER TABLE `tbl_ad_campaign_locations`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_campaign_ad_packages`
--
ALTER TABLE `tbl_campaign_ad_packages`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `tbl_campaign_packages`
--
ALTER TABLE `tbl_campaign_packages`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
